package com.vov4ik;

import com.vov4ik.controller.Controller;

public class Application {
    public static void main(String[] args) {
        Controller controller = new Controller();
        controller.start();
    }
}
