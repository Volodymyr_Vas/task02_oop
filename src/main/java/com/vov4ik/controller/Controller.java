package com.vov4ik.controller;

import com.vov4ik.model.engine.Start;

public class Controller {
    private Start start;

    public Controller() {
        start = new Start();
    }

    public void start() {
        start.showMenu();
    }
}
