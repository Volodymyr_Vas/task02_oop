package com.vov4ik.model.appliances;

import com.vov4ik.model.interfaces.AC;
import com.vov4ik.model.interfaces.DC;

import static com.vov4ik.viev.OutputMessages.*;

public class Laptop implements DC, Comparable<AC> {
    private int power = 360;
    private boolean pluggedIntoSocket = false;
    private boolean switchedOn = false;
    private boolean chargerWorkEnable = true;
    private boolean broken = false;

    @Override
    public String getName() {
        return "Laptop";
    }

    @Override
    public int getPower() {
        return power;
    }

    @Override
    public boolean isSwitchedOn() {
        return switchedOn;
    }


    @Override
    public boolean isPluggedIntoSocket() {
        return pluggedIntoSocket;
    }


    @Override
    public boolean isBroken() {
        return broken;
    }

    @Override
    public boolean canWorkWithACCharger() {
        return chargerWorkEnable;
    }

    @Override
    public void turnOn() {
        if (!switchedOn) {
            switchedOn = true;
            System.out.println(TURN_ON_MESSAGE);
        } else {
            System.out.println(SWITCHED_ON_MESSAGE);
        }
    }

    @Override
    public void turnOff() {
        if (switchedOn) {
            switchedOn = false;
            System.out.println(TURN_OFF_MESSAGE);
        } else {
            System.out.println(SWITCHED_OFF_MESSAGE);
        }
    }

    @Override
    public void repair() {
        if (pluggedIntoSocket) {
            System.out.println(PLUGGED_INTO_SOCKET_REPAIR_MESSAGE);
        } else if (!broken) {
            System.out.println(IS_NOT_BROKEN);
        } else {
            System.out.println(REPAIR_WARNING_MESSAGE);
        }
    }

    @Override
    public void plugIntoSocket() {
        if (!pluggedIntoSocket) {
            pluggedIntoSocket = true;
            System.out.println(PLUGGING_MESSAGE);
        } else {
            System.out.println(PLUGGED_DEVICE_MESSAGE);
        }
    }

    @Override
    public void unplugFromSocket() {
        if (pluggedIntoSocket) {
            pluggedIntoSocket = false;
            System.out.println(UNPLUGGING_MESSAGE);
        } else {
            System.out.println(UNPLUGGED_DEVICE_MESSAGE);
        }
    }

    @Override
    public int compareTo(AC o) {
        return Integer.compare(this.getPower(), o.getPower());
    }
}
