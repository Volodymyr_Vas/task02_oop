package com.vov4ik.model.appliances;

import com.vov4ik.model.interfaces.AC;
import com.vov4ik.model.interfaces.DC;

import static com.vov4ik.viev.OutputMessages.*;

public class Watch implements DC, Comparable<AC> {
    private int power = 10;
    private boolean pluggedIntoSocket = false;
    private boolean switchedOn = true;
    private boolean chargerWorkEnable = false;
    private boolean broken = true;

    @Override
    public String getName() {
        return "Watch";
    }

    @Override
    public int getPower() {
        return power;
    }

    @Override
    public boolean isSwitchedOn() {
        return switchedOn;
    }


    @Override
    public boolean isPluggedIntoSocket() {
        return pluggedIntoSocket;
    }


    @Override
    public boolean isBroken() {
        return broken;
    }

    @Override
    public boolean canWorkWithACCharger() {
        return chargerWorkEnable;
    }

    @Override
    public void turnOn() {
        if (!switchedOn) {
            switchedOn = true;
            System.out.println(TURN_ON_MESSAGE);
        } else {
            System.out.println(SWITCHED_ON_MESSAGE);
        }
    }

    @Override
    public void turnOff() {
        if (switchedOn) {
            switchedOn = false;
            System.out.println(TURN_OFF_MESSAGE);
        } else {
            System.out.println(SWITCHED_OFF_MESSAGE);
        }
    }

    @Override
    public void repair() {
        if (!broken) {
            System.out.println(IS_NOT_BROKEN);
        } else {
            System.out.println(REPAIR_WARNING_MESSAGE);
        }
    }

    @Override
    public void plugIntoSocket() {
        System.out.println(WATCH_PLUG_UNPLUG_MESSAGE);
    }

    @Override
    public void unplugFromSocket() {
        System.out.println(WATCH_PLUG_UNPLUG_MESSAGE);
    }

    @Override
    public int compareTo(AC o) {
        return Integer.compare(this.getPower(), o.getPower());
    }
}
