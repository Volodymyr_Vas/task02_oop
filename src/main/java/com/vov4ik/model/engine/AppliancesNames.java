package com.vov4ik.model.engine;

public enum AppliancesNames {
    Iron,
    TV,
    WaterHeater,
    Watch,
    Laptop
}
