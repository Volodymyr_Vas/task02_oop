package com.vov4ik.model.engine;

enum MenuCondition {
    StartPoint,
    MainMenu,
    DeviceChoosing,
    DeviceSetting,
    SearchingOptions
}
