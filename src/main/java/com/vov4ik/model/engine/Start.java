package com.vov4ik.model.engine;

import com.vov4ik.model.appliances.*;
import com.vov4ik.model.interfaces.AC;
import com.vov4ik.model.interfaces.DC;

import java.util.*;
import java.util.stream.Collectors;

import static com.vov4ik.model.engine.MenuCondition.*;
import static com.vov4ik.viev.MenuMessages.*;

public class Start {
    private String menuConditional = StartPoint.name();
    private String userInput = " ";
    private AC iron = new Iron();
    private AC tv = new TV();
    private AC waterHeater = new WaterHeater();
    private DC watch = new Watch();
    private DC laptop = new Laptop();
    private Scanner scanner = new Scanner(System.in);
    private List<AC> appliancesList;

    //zero initialisation to avoid idea errors messages
    //this values are using in filter
    private int lowerBound = 0;
    private int upperBound = 0;

    public Start() {
        appliancesList = new LinkedList<>();
        appliancesList.add(iron);
        appliancesList.add(tv);
        appliancesList.add(waterHeater);
        appliancesList.add(watch);
        appliancesList.add(laptop);
    }

    public void showMenu() {
        System.out.printf(WELCOME, Arrays.toString(AppliancesNames.values()));
        do {
            if (menuConditional.equals(StartPoint.name())) {
                menuConditional = MainMenu.name();
                checkMenuInput();
            }
            if (menuConditional.equals(DeviceChoosing.name())) {
                showAllDevices();
            }
        } while (!("q".equals(userInput)));
    }

    private void checkMenuInput() {
        System.out.println(MENU);
        do {
            userInput = scanner.nextLine();
            if ("q".equals(userInput)) {
                System.out.println(QUIT_MESSAGE);
                break;
            } else if ("1".equals(userInput)) {
                menuConditional = DeviceChoosing.name();
                break;
            } else if ("2".equals(userInput)) {
                showWorkingDevicesAndPowerUsage();
                menuConditional = StartPoint.name();
            } else if ("3".equals(userInput)) {
                showSortedList();
                menuConditional = StartPoint.name();
            } else if ("4".equals(userInput)) {
                menuConditional = SearchingOptions.name();
                deviceFilterByPower();
            } else {
                System.out.println(INCORRECT_INPUT);
            }
        } while (menuConditional.equals(MainMenu.name()));
    }

    private void showAllDevices() {
        System.out.println(DEVICES_MENU);
        do {
            userInput = scanner.nextLine();
            if ("q".equals(userInput)) {
                System.out.println(QUIT_MESSAGE);
                break;
            } else if ("b".equals(userInput)) {
                menuConditional = StartPoint.name();
                break;
            } else if ("1".equals(userInput)) {
                System.out.printf(YOU_CHOOSE_MESSAGE, iron.getName());
                menuConditional = DeviceSetting.name();
                setChosenDevice(iron);
            } else if ("2".equals(userInput)) {
                System.out.printf(YOU_CHOOSE_MESSAGE, waterHeater.getName());
                menuConditional = DeviceSetting.name();
                setChosenDevice(waterHeater);
            } else if ("3".equals(userInput)) {
                System.out.printf(YOU_CHOOSE_MESSAGE, tv.getName());
                menuConditional = DeviceSetting.name();
                setChosenDevice(tv);
            } else if ("4".equals(userInput)) {
                System.out.printf(YOU_CHOOSE_MESSAGE, laptop.getName());
                menuConditional = DeviceSetting.name();
                setChosenDevice(laptop);
            } else if ("5".equals(userInput)) {
                System.out.printf(YOU_CHOOSE_MESSAGE, watch.getName());
                menuConditional = DeviceSetting.name();
                setChosenDevice(watch);
            } else {
                System.out.println(INCORRECT_INPUT);
                break;
            }
        } while (menuConditional.equals(DeviceChoosing.name()));
    }

    private void showWorkingDevicesAndPowerUsage() {
        int summaryPower = 0;
        List<AC> workingDevices = appliancesList.stream()
                .filter(AC::isSwitchedOn)
                .collect(Collectors.toList());
        if (workingDevices.size() == 0) {
            System.out.println("there's no working devices");
        } else {
            for (AC workingDevice : workingDevices) {
                System.out.println(workingDevice.getName()
                        + " " + workingDevice.getPower() + " Watt");
                summaryPower += workingDevice.getPower();
            }
        }
        System.out.printf(SUMMARY_POWER_MESSAGE, summaryPower);

    }

    private void showSortedList() {
        appliancesList.sort(Comparator.comparingInt(AC::getPower));
        for (AC ac : appliancesList) {
            System.out.println(ac.getName()
                    + " " + ac.getPower() + " Watt");
        }
        System.out.println("\n");
    }

    private void setChosenDevice(AC chosenDevice) {
        do {
            System.out.println(DEVICE_SETTINGS_MENU);
            userInput = scanner.nextLine();
            if ("q".equals(userInput)) {
                System.out.println(QUIT_MESSAGE);
                break;
            } else if ("b".equals(userInput)) {
                menuConditional = DeviceChoosing.name();
                System.out.println(DEVICES_MENU);
                break;
            } else if ("1".equals(userInput)) {
                System.out.println("1");
                chosenDevice.plugIntoSocket();
            } else if ("2".equals(userInput)) {
                chosenDevice.unplugFromSocket();
            } else if ("3".equals(userInput)) {
                chosenDevice.turnOn();
            } else if ("4".equals(userInput)) {
                chosenDevice.turnOff();
            } else if ("5".equals(userInput)) {
                chosenDevice.repair();
            } else {
                System.out.println(INCORRECT_INPUT);
            }
        } while (menuConditional.equals(DeviceSetting.name()));
    }

    private void deviceFilterByPower() {
        System.out.println(FILTER_MESSAGE);
        getLoverBoundInput();
        if (menuConditional.equals((SearchingOptions.name()))) {
            getUpperBoundInput();
        }
    }

    private void getLoverBoundInput() {
        do {
            System.out.println(LOWER_BOUND_INPUT);
            userInput = scanner.nextLine();
            if ("q".equals(userInput)) {
                System.out.println(QUIT_MESSAGE);
                menuConditional = StartPoint.name();
                break;
            } else if ("b".equals(userInput)) {
                menuConditional = StartPoint.name();
            } else {
                try {
                    lowerBound = Integer.parseInt(userInput);
                } catch (NumberFormatException ex) {
                    System.out.println(INCORRECT_INPUT);
                    continue;
                }
                if (lowerBound < 0 || lowerBound > 10000) {
                    System.out.println(INCORRECT_INPUT);
                } else {
                    break;
                }
            }
        } while (menuConditional.equals(SearchingOptions.name()));
    }

    private void getUpperBoundInput() {
        do {
            System.out.println(UPPER_BOUND_INPUT);
            userInput = scanner.nextLine();
            try {
                upperBound = Integer.parseInt(userInput);
            } catch (NumberFormatException ex) {
                System.out.println(INCORRECT_INPUT);
                continue;
            }
            if (upperBound < lowerBound || upperBound > 10000) {
                System.out.println(INCORRECT_INPUT);
            } else {
                break;
            }
        } while (menuConditional.equals(SearchingOptions.name()));
        System.out.println(FILTERED_APPLIANCES);
        appliancesList.stream().filter(device -> device.getPower() > lowerBound)
                .filter(device -> device.getPower() < upperBound)
                .forEach(device -> System.out.println(device.getName()));
        menuConditional = StartPoint.name();
    }
}
