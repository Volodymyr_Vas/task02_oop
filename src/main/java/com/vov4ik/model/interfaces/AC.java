package com.vov4ik.model.interfaces;

public interface AC {
    String getName();

    int getPower();

    boolean isSwitchedOn();

    boolean isPluggedIntoSocket();

    boolean isBroken();

    void turnOn();

    void turnOff();

    void repair();

    void plugIntoSocket();

    void unplugFromSocket();
}
