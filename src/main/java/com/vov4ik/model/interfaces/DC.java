package com.vov4ik.model.interfaces;

public interface DC extends AC {
    boolean canWorkWithACCharger();
}
