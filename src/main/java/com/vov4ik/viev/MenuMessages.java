package com.vov4ik.viev;

public class MenuMessages {
    public static final String WELCOME = "Welcome to our app. Here you can see all devices in "
            + "your home \nand choose one of it. \nList of devices:\n%s";
    public final static String MENU = "\nMENU: \n1. Choose device. \n2. See working devices."
            + "\n3. Sort all devices by their power. \n4. Search devices by options. \nq. Quit";
    public final static String DEVICES_MENU = "Choose one of these devices: \n1. Iron. "
            + "\n2. WaterHeater. \n3. TV. \n4. Laptop. \n5. Watch. \nb. Back \nq. Quit";
    public static final String QUIT_MESSAGE = "Good bye, have a good day";
    public static final String SUMMARY_POWER_MESSAGE = "Summary power using is %d Watt\n\n";
    public static final String INCORRECT_INPUT = "Your input is incorrect. Please, try again";
    public static final String YOU_CHOOSE_MESSAGE = "You choose %s.\n";
    public static final String DEVICE_SETTINGS_MENU = "\nDevice settings: \n1. Plug into socket."
            + "\n2. Unplug from socket. \n3. Turn on. \n4. Turn off. \n5. Repair. \nb. Back \nq. Quit";
    public static final String FILTER_MESSAGE = "You can search devices with power from 0 to 10 000 Watt.";
    public static final String LOWER_BOUND_INPUT = "Please, enter lower bound: or q or b";
    public static final String UPPER_BOUND_INPUT = "Please, enter upper bound:";
    public static final String FILTERED_APPLIANCES = "Appliances that we found:";
}
