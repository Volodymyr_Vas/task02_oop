package com.vov4ik.viev;

public class OutputMessages {
    public final static String SWITCHED_ON_MESSAGE = "This device is already switched on";
    public final static String TURN_ON_MESSAGE = "Turning on... Success.\n"
            + "Your device is switched on";
    public final static String SWITCHED_OFF_MESSAGE = "This device is already switched off";
    public final static String TURN_OFF_MESSAGE = "Turning off... Success.\n"
            + "Your device is switched off";
    public final static String UNPLUGGED_DEVICE_MESSAGE = "Your device is already unplugged";
    public final static String PLUGGED_DEVICE_MESSAGE = "Your device is already plugged";
    public final static String UNPLUGGING_MESSAGE = "Unplugging... Success.\nYour device is "
            + "now unplugged";
    public final static String PLUGGING_MESSAGE = "Plugging... Success. \nYour device is "
            + "now plugged into socket";
    public final static String UNPLUGGED_DEVICE_REPORT = "Your device is unplugged from socket. \n"
            + "You should plug it into socket before using";
    public final static String PLUGGED_INTO_SOCKET_REPAIR_MESSAGE = "You "
            + "should unplug your device "
            + "from socket before repairing";
    public final static String IS_NOT_BROKEN = "This device isn't broken";
    public final static String REPAIR_WARNING_MESSAGE = "You shouldn't repair this device by yourself. \n"
            + "Please, visit our service center";
    public final static String WATCH_PLUG_UNPLUG_MESSAGE = "You can't do it with the device using only DC";
    public final static String NO_WORKING_DEVICES_MESSAGES = "You have no working devices now. \n"
            + "You can turn them on from menu";
}
